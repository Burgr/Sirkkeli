﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirkkeli
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Circle c1 = new Circle(5.0);
            Console.WriteLine(c1.ToString());
            Console.WriteLine("The color of the circle is " + c1.getColor());

            Circle c2 = new Circle(1.2);
            Console.WriteLine(c2.ToString());
            Console.WriteLine(c2);
            Console.WriteLine("Operator '+' invokes toString() too: " + c2);

            Circle c3 = new Circle(3.0, "blue");
            Console.WriteLine("The circle has radius of "+ c3.getRadius() + " and area of " + c3.getArea());
            Console.WriteLine("The color of the circle is " + c3.getColor());

            Circle c4 = new Circle();
            c4.setRadius(5.0);
            Console.WriteLine("Radius is: " + c4.getRadius());
            c4.setColor("green");
            Console.WriteLine("Color is: " + c4.getColor());

            Employee e1 = new Employee();
            Console.WriteLine(e1.ToString());

            Date d1 = new Date();
            Console.WriteLine(d1.ToString());

            Date d2 = new Date();
            d2.setDate(2, 3, 2005);
            Console.WriteLine(d2.getDay() + " " + d2.getMonth() + " " + d2.getYear());

            Time t1 = new Time();
            Console.WriteLine(t1.ToString());
        }
    }
}
