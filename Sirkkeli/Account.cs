﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirkkeli
{
    public class Account
    {
        private String id;
        private String name;
        private int balance = 0;

        public Account()
        {
            id = "AccountYksi";
            name = "Marakassi";
        }

        public Account(int balance)
        {
            id = "AccountKaksi";
            name = "Kapitain";
            balance = 5000;
        }

        public String getID()
        {
            return id;
        }

        public String getName()
        {
            return name;
        }

        public int getBalance()
        {
            return balance;
        }

        public int credit(int amount)
        {
            return balance = balance + amount;
        }

        public int debit(int amount)
        {
            if (amount <= balance)
            {
                return balance = balance - amount;
            }
            else
            {
                Console.WriteLine("Amount exceeded balance");
                return balance;
            }
        }

        public override string ToString()
        {
            return "Account[id=" + id + "name=" + name + "balance=" + balance + "]";
        }
    }
}
