﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirkkeli
{
    public class InvoiceItem
    {
        private String id;
        private String desc;
        private int qty;
        private double unitPrice;

        public String getID()
        {
            return id;
        }

        public String getDesc()
        {
            return desc;
        }

        public int getQty()
        {
            return qty;
        }

        public void setQty(int qty)
        {
            this.qty = qty;
        }

        public double getUnitPrice()
        {
            return unitPrice;
        }

        public void setUnitPrice(double unitPrice)
        {
            this.unitPrice = unitPrice;
        }

        public double getTotal()
        {
            return unitPrice * qty;
        }

        public override string ToString()
        {
            return "InvoiceItem[id=" + id + "desc=" + desc + "qty=" + qty + "unitPrice" + unitPrice + "]";
        }
    }
}
