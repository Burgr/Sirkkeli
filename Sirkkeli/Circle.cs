﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirkkeli
{
    public class Circle
    {
        private double radius;
        private String color;

        public Circle()
        {
            this.radius = 1.0;
            this.color = "red";
        }

        public Circle(double r)
        {
            this.radius = r;
            this.color = "white";
        }

        public Circle(double r, String color)
        {
            this.radius = r;
            this.color = color;

        }

        public double getRadius()
        {
            return radius;
        }

        public double getArea()
        {
            return radius * radius * Math.PI;
        }

        public String getColor()
        {
            return color;
        }

        public void setRadius(double newRadius)
        {
            this.radius = newRadius;
        }

        public void setColor(String color)
        {
            this.color = color;
        }

        public override String ToString()
        {
            return "Circle[radius=" + radius + " color=" + color + "]";
        }
    }
}
