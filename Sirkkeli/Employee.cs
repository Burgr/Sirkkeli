﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirkkeli
{
    public class Employee
    {
        private int id;
        private int salary;
        private String firstName;
        private String lastName;

        public Employee(int id, String firstName, String lastName, int salary)
        {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.salary = salary;
        }

        public Employee()
        {
            id = 666666;
            firstName = "Itse";
            lastName = "Piru";
            salary = 666;
        }

        public int getID()
        {
            return id;
        }

        public String getFirstName()
        {
            return firstName;
        }

        public String getLastName()
        {
            return lastName;
        }

        public int getSalary()
        {
            return salary;
        }

        public String getName()
        {
            return firstName + " " + lastName;
        }

        public void setSalary(int salary)
        {
            this.salary = salary;
        }

        public int getAnnualSalary()
        {
            return salary * 12;
        }

        public override string ToString()
        {
            return "Employee[id=" + id + "name=" + firstName + lastName + "salary=" + salary + "]";
        }
    }
}
