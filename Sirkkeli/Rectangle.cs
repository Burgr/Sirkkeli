﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirkkeli
{
    public class Rectangle
    {
        private float length;
        private float width;

        public Rectangle()
        {
            length = 1.0f;
            width = 1.0f;
        }

        public float getLength()
        {
            return length;
        }

        public float getWidth()
        {
            return width;
        }

        public void setLength(float length)
        {
            this.length = length;
        }

        public void setWidth(float width)
        {
            this.width = width;
        }

        public double getArea()
        {
            return width * length;
        }

        public double getPerimeter()
        {
            return width * 2 + length * 2;
        }
    }
}
