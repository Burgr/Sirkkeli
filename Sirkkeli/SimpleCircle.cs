﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirkkeli
{
    public class SimpleCircle
    {
        private double radius;

        public override string ToString()
        {
            return "Circle[radius=" + radius + "]";
        }

        public SimpleCircle()
        {
            radius = 1.0;
        }

        public SimpleCircle(double radius)
        {
            this.radius = radius;
        }

        public double getRadius()
        {
            return radius;
        }

        public void setRadius(double radius)
        {
            this.radius = radius;
        }

        public double getArea()
        {
            return radius * radius * Math.PI;
        }

        public double getCircumference()
        {
            return 2 * Math.PI * radius;
        }
    }
}
