﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirkkeli
{
    public class Time
    {
        private int hour;
        private int minute;
        private int second;

        public Time()
        {
            hour = 2;
            minute = 15;
            second = 32;
        }

        public Time(int hour, int minute, int second)
        {
            this.hour = hour;
            this.minute = minute;
            this.second = second;
        }

        public int getHour()
        {
            return hour;
        }

        public int getMinute()
        {
            return minute;
        }

        public int getSecond()
        {
            return second;
        }

        public void setHour(int hour)
        {
            this.hour = hour;
        }
        
        public void setMinute(int minute)
        {
            this.minute = minute;
        }

        public void setSecond(int second)
        {
            this.second = second;
        }

        public void setTime(int hour, int minute, int second)
        {
            this.hour = hour;
            this.minute = minute;
            this.second = second;
        }

        public override string ToString()
        {
            return hour + ":" + minute + ":" + second;
        }

        public int NextSecond()
        {
            return second += 1;
        }

        public int PreviousSecond()
        {
            return second -= 1;
        }
    }
}
